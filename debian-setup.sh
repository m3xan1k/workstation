sudo apt update && sudo apt upgrade

sudo apt install \
    firmware-amd-graphics \
    firmware-misc-nonfree \
    dconf-cli \
    dconf-editor \
    git \
    libncurses-dev \
    xclip \
    xsel \
    neofetch \
    ripgrep \
    fzf \
    wireguard \
    filezilla \
    keepassxc \
    mpv \
    vlc \
    flameshot \
    python3-pip \
    pipx \
    libx11-dev \
    libxft-dev \
    ninja-build \
    gettext \
    cmake \
    unzip \
    curl \
    clangd-15 \
    lz4 \
    build-essential \
    vim \
    ca-certificates \
    gnupg \
    iptables \
    python3-rich \
    libffi-dev \
    libgdbm-dev \
    libncurses5-dev \
    libnspr4-dev \
    libnss3-dev \
    libreadline-dev \
    libnspr4-dev \
    pkg-config \
    libfreetype6-dev \
    libfontconfig1-dev \
    libxcb-xfixes0-dev \
    libxkbcommon-dev \
    tmux \
    speech-dispatcher \
    resolvconf \
    lightdm-settings \
    zip \
    unzip \
    engrampa \
    gstreamer1.0-plugins-ugly \
    caja-open-terminal \
    mate-system-monitor \
    stow \
    scrot \
    i3 \
    yt-dlp \
    lightdm-settings \
    rofi \
    emacs \
    libssl-dev \
    xfce4-power-manager \
    rsync \
    tk-dev \
    libvterm-dev \
    shadowsocks-libev

sudo apt remove -y mate-power-manager

# for intel
# wget https://github.com/thesofproject/sof-bin/releases/download/v2024.03/sof-bin-2024.03.tar.gz
# tar -xvf sof-bin-2024.03.tar.gz
# cd sof-bin-2024.03.tar.gz
# sudo ./install.sh

# c language server
sudo ln -s /usr/bin/clangd-15 /usr/bin/clangd

# dotfiles and links
git clone git@codeberg.org:m3xan1k/dotfiles.git ~/dotfiles

rm ~/.bashrc
cd ~/dotfiles
stow home
stow nvim
stow i3
stow rofi
stow tmux
stow scripts
cd

# TODO: dunstrc

# pyradio
# wget https://raw.githubusercontent.com/coderholic/pyradio/master/pyradio/install.py
# python3 install.py

# global python things
pipx install flake8
pipx install pipenv
pipx install ipython

mkdir ~/soft
mkdir ~/.local/share/fonts

# golang
curl -L https://go.dev/dl/go1.20.5.linux-amd64.tar.gz -o ~/soft/go1.20.5.tar.gz
sudo tar -C /usr/local -xzf ~/soft/go1.20.5.tar.gz

# VIMIX theme
git clone https://github.com/m3xan1k/ghostbsd-wallpapers ~/soft/ghostbsd-wallpapers
git clone git@codeberg.org:m3xan1k/ghostbsd-mate-themes.git ~/soft/ghostbsd-mate-themes
git clone git@codeberg.org:m3xan1k/ghostbsd-icons.git ~/soft/ghostbsd-icons

echo "Installing themes..."
    if [ ! -d ~/.themes ]; then     mkdir ~/.themes; fi
cp -R ~/soft/ghostbsd-mate-themes/themes/* ~/.themes

echo "Installing icons..."
    if [ ! -d ~/.icons ]; then   mkdir ~/.icons; fi
cp -R ~/soft/ghostbsd-icons/icons/* ~/.icons

# fonts
echo "Intalling fonts..."
curl -L https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/NerdFontsSymbolsOnly.zip -o ~/soft/nerdfonts.zip
unzip ~/soft/nerdfonts.zip -d ~/soft/nerdfonts
cp ~/soft/nerdfonts/SymbolsNerdFontMono-Regular.ttf ~/.local/share/fonts/
cp ~/soft/nerdfonts/SymbolsNerdFont-Regular.ttf ~/.local/share/fonts/
cp ~/dotfiles/fonts/* ~/.local/share/fonts/
fc-cache -f -v

# Building Neovim
# https://github.com/neovim/neovim/wiki/Building-Neovim
git clone https://github.com/neovim/neovim ~/soft/neovim
cd ~/soft/neovim
git checkout stable
make CMAKE_BUILD_TYPE=RelWithDebInfo
cd ~/soft/neovim/build && cpack -G DEB && sudo dpkg -i ~/soft/neovim/build/nvim-linux64.deb

# Telegram Desktop
curl -L https://telegram.org/dl/desktop/linux -o ~/Downloads/telegram.tar.xz
tar -xvf ~/Downloads/telegram.tar.xz

# docker
curl -L https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/containerd.io_1.6.21-1_amd64.deb -o ~/soft/containerd.io_1.6.21-1_amd64.deb
curl -L https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/docker-buildx-plugin_0.10.5-1~debian.12~bookworm_amd64.deb -o ~/soft/docker-buildx-plugin_0.10.5-1~debian.12~bookworm_amd64.deb
curl -L https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/docker-ce-cli_24.0.2-1~debian.12~bookworm_amd64.deb -o ~/soft/docker-ce-cli_24.0.2-1~debian.12~bookworm_amd64.deb
curl -L https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/docker-ce_24.0.2-1~debian.12~bookworm_amd64.deb -o ~/soft/docker-ce_24.0.2-1~debian.12~bookworm_amd64.deb
curl -L https://download.docker.com/linux/debian/dists/bookworm/pool/stable/amd64/docker-compose-plugin_2.18.1-1~debian.12~bookworm_amd64.deb -o ~/soft/docker-compose-plugin_2.18.1-1~debian.12~bookworm_amd64.deb

# may fail cause of iptables
sudo dpkg -i ~/soft/containerd.io_1.6.21-1_amd64.deb \
    ~/soft/docker-ce_24.0.2-1~debian.12~bookworm_amd64.deb \
    ~/soft/docker-ce-cli_24.0.2-1~debian.12~bookworm_amd64.deb \
    ~/soft/docker-buildx-plugin_0.10.5-1~debian.12~bookworm_amd64.deb \
    ~/soft/docker-compose-plugin_2.18.1-1~debian.12~bookworm_amd64.deb

# dbgate
curl -L https://github.com/dbgate/dbgate/releases/download/v5.2.6/dbgate-5.2.6-linux_amd64.deb -o ~/soft/dbgate.deb
sudo dpkg -i ~/soft/dbgate.deb

# insomnia
curl -L https://github.com/Kong/insomnia/releases/download/core@2023.2.2/Insomnia.Core-2023.2.2.deb -o ~/soft/insomnia.deb
sudo dpkg -i ~/soft/insomnia.deb

# emacs 29
sudo apt install -y \
    build-essential \
    libgtk-3-dev \
    libgnutls28-dev \
    libtiff5-dev \
    libgif-dev \
    libjpeg-dev \
    libpng-dev \
    libxpm-dev \
    libncurses-dev \
    libwebkit2gtk-4.0-dev \
    texinfo \
    libjansson4 \
    libjansson-dev \
    libgccjit0 \
    libgccjit-12-dev \
    libmagickcore-dev \
    libmagick++-dev \
    libtree-sitter-dev

git clone --depth 1 --branch emacs-29 git://git.savannah.gnu.org/emacs.git ~/soft/emacs

cd ~/soft/emacs
./autogen.sh \
    && ./configure \
    --with-native-compilation \
    --with-x-toolkit=gtk3 \
    --with-tree-sitter \
    --with-wide-int \
    --with-json \
    --with-modules \
    --without-dbus \
    --with-gnutls \
    --with-mailutils \
    --without-pop \
    --with-cairo \
    --with-imagemagick

# make -j${nproc}
# sudo make install

git clone git@codeberg.org:m3xan1k/.emacs.d ~/.emacs.d

# Nextcloud
curl -L https://github.com/nextcloud-releases/desktop/releases/download/v3.15.3/Nextcloud-3.15.3-x86_64.AppImage -o ~/soft/nextcloud.AppImage
chmod +x ~/soft/nextcloud.AppImage
# ./soft/nextcloud.AppImage

# asdf
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.14.0

# load dconf
dconf load /org/mate/ < ~/dotfiles/mate.dconf

# grub
echo 'setting up grub...'
sudo bash -c "echo 'GRUB_BACKGROUND=\"\"' >> /etc/default/grub"
sudo bash -c "echo 'set menu_color_normal=white/black' >> /boot/grub/custom.cfg"
sudo bash -c "echo 'set menu_color_highlight=black/light-gray' >> /boot/grub/custom.cfg"
sudo update-grub

# disable thinkpad beeper
sudo bash -c "echo 'blacklist pcspkr' >> /etc/modprobe.d/blacklist-local.conf"

echo "Install rust manually run: curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh"
# rust
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# setup emacs-lsp-booster
# git clone https://github.com/blahgeek/emacs-lsp-booster
# cd emacs-lsp-booster
# cargo build --release
# cp target/release/emacs-lsp-booster ~/.local/bin/
