# Automation for my workstation setup

1. Minimal pre-setup

```
sudo apt update && sudo apt upgrade

sudo apt install git keepassxc
```

2. Set up ssh keys from private storage

```
chmod 600 ~/.ssh/id_rsa
```

3. Clone this repo

```
git clone git@codeberg.org:m3xan1k/workstation
```

Run main script

```
cd workstation
./debian-setup.sh
```

4. Set up lightdm-settings in GUI

5. Set up notifications with dunst

**/usr/share/dbus-1/services/org.freedesktop.mate.Notifications.service**

```
[D-BUS Service]
Name=org.freedesktop.Notifications
Exec=/usr/bin/dunst
AssumedAppArmorLabel=unconfined
```

6. Set up Nextcloud app in **~/soft/nextcloud.AppImage**

7. Set up Telegram app in **~/Downloads/Telegram/Telegram**

8. Get Slack [from here](https://slack.com/downloads/linux)

9. Older versions of python

```
asdf plugin-add python

asdf install python 3.8.19
```