mkdir soft

echo "Installing xcode tools..."
xcode-select --install
echo "Done"

echo "Bootstrapping homebrew..."
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# changing gui applications path
sudo sh -c 'echo "applications_dir ~/Applications" >> /opt/local/etc/macports/macports.conf'
echo "Done"

echo "Bootstrapping pkgsrc..."
export BOOTSTRAP_TAR="bootstrap-macos12.3-trunk-arm64-20240426.tar.gz"
sudo tar -zxpf ~/soft/${BOOTSTRAP_TAR} -C /
eval $(/usr/libexec/path_helper)
echo "Done"

echo "Installing soft and tools from brew..."
brew install \
    shadowsocks-libev \
    wireguard-go \
    bash-completion@2 \
    libpq

# spell
brew install --cask wkhtmltopdf xquartz
brew install postgresql@15 libxml2 libxslt jpeg-turbo zlib libffi cairo libmagic redis nginx

# python
brew install pkg-config openssl@3 xz gdbm tcl-tk mpdecimal
echo "Done"

echo "Installing insomnia..."
hdiutil attach ~/soft/Insomnia.Core-2023.4.0.dmg
cp -R /Volumes/Insomnia\ 2023.4.0-universal/Insomnia.app ~/Applications
hdiutil detach /Volumes/Insomnia\ 2023.4.0-universal
echo "Done"

echo "Installing Firefox..."
hdiutil attach ~/soft/Firefox\ 131.0.3.dmg
cp -R /Volumes/Firefox/Firefox.app ~/Applications
hdiutil detach /Volumes/Firefox
echo "Done"

echo "Installing Firefox Developer Edition..."
hdiutil attach ~/soft/Firefox\ 133.0b3.dmg
cp -R /Volumes/Firefox\ Developer\ Edition/Firefox\ Developer\ Edition.app ~/Applications
hdiutil detach /Volumes/Firefox\ Developer\ Edition/
echo "Done"

echo "Installing Amethyst..."
unzip Amethyst.zip
cp -R ~/soft/Amethyst.app ~/Applications
echo "Done"

echo "Installing Postico2..."
hdiutil attach ~/soft/postico-9702.dmg
cp -R /Volumes/Postico\ 2.1.2\ \(9702\)/Postico\ 2.app ~/Applications/
hdiutil detach /Volumes/Postico\ 2.1.2\ \(9702\)/
echo "Done"

echo "Installing Orbstack..."
hdiutil attach ~/soft/OrbStack_v1.7.5_18165_arm64.dmg
cp -R /Volumes/Install\ OrbStack\ v1.7.5/OrbStack.app ~/Applications
hdiutil detach /Volumes/Install\ OrbStack\ v1.7.5/
echo "Done"

echo "Installing KeePassXC..."
hdiutil attach ~/soft/KeePassXC-2.7.9-arm64.dmg
cp -R /Volumes/KeePassXC/KeePassXC.app ~/Applications/
hdiutil detach /Volumes/KeePassXC/
echo "Done"

echo "Installing Golang..."
curl -L https://go.dev/dl/go1.23.2.darwin-arm64.pkg -o ~/soft/go1.23.2.darwin-arm64.pkg
sudo installer -pkg ~/soft/go1.23.2.darwin-arm64.pkg -target /
echo "Done"

echo "Installing Rust..."
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
echo "Done"

echo "Installing Fish..."
sudo installer -pkg ~/soft/fish-3.7.1.pkg -target /
echo "Done"

echo "Installing Nextcloud..."
installer -pkg ~/soft/Nextcloud-3.14.2.pkg -target CurrentUserHomeDirectory
echo "Done"

echo "Installing VLC..."
hdiutil attach ~/soft/vlc-3.0.21-arm64.dmg
cp -R /Volumes/VLC\ media\ player/VLC.app ~/Applications/
hdiutil detach /Volumes/VLC\ media\ player/
echo "Done"

echo "Installing FileZilla..."
cp -R ~/soft/FileZilla.app ~/Applications/
echo "Done"

echo "Installing Emacs..."
hdiutil attach ~/soft/Emacs-29.4-1-universal.dmg
cp -R /Volumes/Emacs/Emacs.app ~/Applications/
hdiutil detach /Volumes/Emacs/
echo "Done"

echo "Installing additional tools within pkgsrc..."
sudo pkgin install -y ripgrep fzf stow gmake coreutils password-store wireguard-tools
echo "Done"

echo "Installing asdf..."
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.14.1
echo "Done"

git clone git@codeberg.org:m3xan1k/.emacs.d ~/.emacs.d

# setup emacs-lsp-booster
# git clone https://github.com/blahgeek/emacs-lsp-booster
# cd emacs-lsp-booster
# cargo build --release
# cp target/release/emacs-lsp-booster ~/.emacs.d/bin/
