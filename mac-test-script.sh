#!/bin/bash

# Exit on error
set -e

# Create directories
mkdir -p ~/soft ~/Applications

# Helper functions
log() {
    echo "==> $1..."
}

install_dmg() {
    local dmg_path="$1"
    local volume_name="$2"
    local app_name="$3"
    
    log "Installing ${app_name}"
    hdiutil attach "$dmg_path"
    cp -R "/Volumes/${volume_name}/${app_name}" ~/Applications
    hdiutil detach "/Volumes/${volume_name}"
    echo "Done"
}

# Install Xcode Command Line Tools
if ! xcode-select -p &>/dev/null; then
    log "Installing Xcode Command Line Tools"
    xcode-select --install
    # Wait for installation to complete
    until xcode-select -p &>/dev/null; do
        sleep 1
    done
fi

# Install MacPorts
log "Bootstrapping MacPorts"
sudo installer -pkg MacPorts-2.10.2-14-Sonoma.pkg -target /

# Configure MacPorts
log "Configuring MacPorts"
if ! grep -q "applications_dir ~/Applications" /opt/local/etc/macports/macports.conf; then
    sudo sh -c 'echo "applications_dir ~/Applications" >> /opt/local/etc/macports/macports.conf'
fi

# Install pkgsrc
log "Bootstrapping pkgsrc"
BOOTSTRAP_TAR="bootstrap-macos12.3-trunk-arm64-20240426.tar.gz"
BOOTSTRAP_SHA="97cbb189458ff2765dbacbbaa691460a1d658e7c"
if [ ! -f "${BOOTSTRAP_TAR}" ]; then
    curl -O "https://pkgsrc.smartos.org/packages/Darwin/bootstrap/${BOOTSTRAP_TAR}"
fi
echo "${BOOTSTRAP_SHA}  ${BOOTSTRAP_TAR}" | shasum -c-
sudo tar -zxpf "${BOOTSTRAP_TAR}" -C /
eval "$(/usr/libexec/path_helper)"

# MacPorts installations
log "Installing software from MacPorts"
sudo port -N install \
    python38 \
    python312 \
    neovim \
    shadowsocks-libev \
    asdf \
    emacs-app

# DMG installations
declare -A apps=(
    ["~/soft/Insomnia.Core-2023.4.0.dmg"]="Insomnia 2023.4.0-universal|Insomnia.app"
    ["~/soft/Firefox 131.0.3.dmg"]="Firefox|Firefox.app"
    ["~/soft/Firefox 133.0b3.dmg"]="Firefox Developer Edition|Firefox Developer Edition.app"
    ["~/soft/postico-9702.dmg"]="Postico 2.1.2 (9702)|Postico 2.app"
    ["~/soft/OrbStack_v1.7.5_18165_arm64.dmg"]="Install OrbStack v1.7.5|OrbStack.app"
    ["~/soft/KeePassXC-2.7.9-arm64.dmg"]="KeePassXC|KeePassXC.app"
    ["~/soft/vlc-3.0.21-arm64.dmg"]="VLC media player|VLC.app"
)

for dmg in "${!apps[@]}"; do
    IFS="|" read -r volume app <<< "${apps[$dmg]}"
    install_dmg "$dmg" "$volume" "$app"
done

# Simple app copies
log "Installing simple apps"
for app in "Amethyst.app" "FileZilla.app"; do
    cp -R ~/soft/"$app" ~/Applications/
done

# Clone Emacs config
log "Setting up Emacs configuration"
git clone git@codeberg.org:m3xan1k/.emacs.d ~/.emacs.d

# Install Go
log "Installing Golang"
curl -L https://go.dev/dl/go1.23.2.darwin-arm64.pkg -o ~/soft/go1.23.2.darwin-arm64.pkg
installer -pkg ~/soft/go1.23.2.darwin-arm64.pkg -target CurrentUserHomeDirectory

# Install Rust
log "Installing Rust"
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

# Install Fish
log "Installing Fish"
sudo installer -pkg ~/soft/fish-3.7.1.pkg -target /

# Install Nextcloud
log "Installing Nextcloud"
sudo installer -pkg ~/soft/Nextcloud-3.14.2.pkg -target CurrentUserHomeDirectory

# Install pkgsrc tools
log "Installing additional tools with pkgsrc"
sudo pkgin install -y ripgrep fzf stow gmake coreutils password-store

log "Installation complete!"